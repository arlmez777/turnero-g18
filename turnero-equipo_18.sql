--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

-- Started on 2022-12-24 19:04:33

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 217 (class 1259 OID 41100)
-- Name: apertura_cierre_boxes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.apertura_cierre_boxes (
    box_id integer NOT NULL,
    fecha_hora_apertura timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    fecha_hora_cierre timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    usuario_id integer NOT NULL
);


ALTER TABLE public.apertura_cierre_boxes OWNER TO postgres;

--
-- TOC entry 3430 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE apertura_cierre_boxes; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.apertura_cierre_boxes IS 'Control de apertura y cierre de BOXES.';


--
-- TOC entry 3431 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN apertura_cierre_boxes.box_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.apertura_cierre_boxes.box_id IS 'Referencia al BOX involucrado.';


--
-- TOC entry 3432 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN apertura_cierre_boxes.fecha_hora_apertura; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.apertura_cierre_boxes.fecha_hora_apertura IS 'Fecha y hora en la que se hace la apertura. Este valor es asignado mediante un DEFAUT para el atributo.';


--
-- TOC entry 3433 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN apertura_cierre_boxes.fecha_hora_cierre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.apertura_cierre_boxes.fecha_hora_cierre IS 'Fecha y hora en que se cierra la atención desde el BOX.';


--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN apertura_cierre_boxes.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.apertura_cierre_boxes.usuario_id IS 'Identificador del registro del usuario asignado al BOX.';


--
-- TOC entry 215 (class 1259 OID 41087)
-- Name: boxes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boxes (
    box_id integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    abreviatura character varying(10) NOT NULL,
    esta_activo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.boxes OWNER TO postgres;

--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE boxes; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.boxes IS 'Boxes disponibles para la atención.';


--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN boxes.box_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.boxes.box_id IS 'Identificador de registro.';


--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN boxes.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.boxes.descripcion IS 'Descripción correspondiente al BOX.';


--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN boxes.abreviatura; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.boxes.abreviatura IS 'Abreviatura a utilizar para el BOX.';


--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN boxes.esta_activo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.boxes.esta_activo IS 'Bandera de actividad del BOX.';


--
-- TOC entry 214 (class 1259 OID 41086)
-- Name: boxes_box_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.boxes_box_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.boxes_box_id_seq OWNER TO postgres;

--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 214
-- Name: boxes_box_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.boxes_box_id_seq OWNED BY public.boxes.box_id;


--
-- TOC entry 216 (class 1259 OID 41093)
-- Name: boxes_servicios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.boxes_servicios (
    box_id integer NOT NULL,
    servicio_id integer NOT NULL
);


ALTER TABLE public.boxes_servicios OWNER TO postgres;

--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE boxes_servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.boxes_servicios IS 'Servicios que puede atender un box.';


--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN boxes_servicios.box_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.boxes_servicios.box_id IS 'Identificador del BOX asociado.';


--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN boxes_servicios.servicio_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.boxes_servicios.servicio_id IS 'Identificador del servicio que puede atender.';


--
-- TOC entry 220 (class 1259 OID 41112)
-- Name: estados; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estados (
    estado_id integer NOT NULL,
    cod_estado character varying(10) NOT NULL,
    descripcion character varying(50) NOT NULL,
    esta_activo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.estados OWNER TO postgres;

--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE estados; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.estados IS 'Estados posibiles para un servicio. Esta tabla es interna al sistema y cuenta con un identificador de registro que no es auto generado.';


--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN estados.estado_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estados.estado_id IS 'Identificador de registro.';


--
-- TOC entry 3446 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN estados.cod_estado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estados.cod_estado IS 'Código o abreviatura para el estado. Este valor se unirá con el número de secuencia del servicio para obtener el código del ticket.';


--
-- TOC entry 3447 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN estados.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estados.descripcion IS 'Nombre o descripción para el estado.';


--
-- TOC entry 3448 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN estados.esta_activo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.estados.esta_activo IS 'Bandera de actividad para el estado.';


--
-- TOC entry 211 (class 1259 OID 41069)
-- Name: prioridades; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.prioridades (
    prioridad_id integer DEFAULT 0 NOT NULL,
    descripcion character varying(50) NOT NULL,
    prioridad integer NOT NULL
);


ALTER TABLE public.prioridades OWNER TO postgres;

--
-- TOC entry 3449 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE prioridades; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.prioridades IS 'Prioridades de atención de los tickets.';


--
-- TOC entry 3450 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN prioridades.prioridad_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.prioridades.prioridad_id IS 'Identificador de registro. No es auto generado.';


--
-- TOC entry 3451 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN prioridades.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.prioridades.descripcion IS 'Descripción para la prioridad.';


--
-- TOC entry 3452 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN prioridades.prioridad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.prioridades.prioridad IS 'Nivel de prioridad de atención.';


--
-- TOC entry 210 (class 1259 OID 41068)
-- Name: prioridades_prioridad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.prioridades_prioridad_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prioridades_prioridad_id_seq OWNER TO postgres;

--
-- TOC entry 3453 (class 0 OID 0)
-- Dependencies: 210
-- Name: prioridades_prioridad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.prioridades_prioridad_id_seq OWNED BY public.prioridades.prioridad_id;


--
-- TOC entry 213 (class 1259 OID 41080)
-- Name: servicios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.servicios (
    servicio_id integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    abreviatura character varying(10) NOT NULL,
    esta_activo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.servicios OWNER TO postgres;

--
-- TOC entry 3454 (class 0 OID 0)
-- Dependencies: 213
-- Name: TABLE servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.servicios IS 'Tabla de definición de los servicios disponibles.';


--
-- TOC entry 3455 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN servicios.servicio_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.servicios.servicio_id IS 'Identificador de registro.';


--
-- TOC entry 3456 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN servicios.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.servicios.descripcion IS 'Nombre o descripción del servicio.';


--
-- TOC entry 3457 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN servicios.abreviatura; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.servicios.abreviatura IS 'Abreviatura para el servicio en cuestión.';


--
-- TOC entry 3458 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN servicios.esta_activo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.servicios.esta_activo IS 'Bandera de disponibilidad del servicio.';


--
-- TOC entry 212 (class 1259 OID 41079)
-- Name: servicios_servicio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.servicios_servicio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.servicios_servicio_id_seq OWNER TO postgres;

--
-- TOC entry 3459 (class 0 OID 0)
-- Dependencies: 212
-- Name: servicios_servicio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.servicios_servicio_id_seq OWNED BY public.servicios.servicio_id;


--
-- TOC entry 221 (class 1259 OID 41117)
-- Name: siguiente_nro_servicio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.siguiente_nro_servicio (
    servicio_id integer NOT NULL,
    fecha date NOT NULL,
    nro_siguiente integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.siguiente_nro_servicio OWNER TO postgres;

--
-- TOC entry 3460 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE siguiente_nro_servicio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.siguiente_nro_servicio IS 'Tabla de mantenimiento de los números a asignar secuencialmente a los servicios a prestar.';


--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN siguiente_nro_servicio.servicio_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.siguiente_nro_servicio.servicio_id IS 'Identificador de registro.';


--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN siguiente_nro_servicio.fecha; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.siguiente_nro_servicio.fecha IS 'Fecha para la que se maneja el numerador.';


--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN siguiente_nro_servicio.nro_siguiente; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.siguiente_nro_servicio.nro_siguiente IS 'Siguiente número a asignar a un pedido de servicio entrante.';


--
-- TOC entry 209 (class 1259 OID 41065)
-- Name: socios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socios (
    socio_id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    nro_documento character varying(20) NOT NULL
);


ALTER TABLE public.socios OWNER TO postgres;

--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE socios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.socios IS 'Es la tabla de los socios. Pertene al un sub sistema diferente.';


--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN socios.socio_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.socios.socio_id IS 'Identificador de registro de la tabla de socios.';


--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN socios.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.socios.nombre IS 'Nombre del socio.';


--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN socios.nro_documento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.socios.nro_documento IS 'Número de documento de identidad del socio.';


--
-- TOC entry 219 (class 1259 OID 41106)
-- Name: tickets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tickets (
    ticket_id integer NOT NULL,
    cod_ticket character varying(20),
    fecha_hora_ingreso_registro timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    nro_documento character varying(20) NOT NULL,
    prioridad_id integer NOT NULL
);


ALTER TABLE public.tickets OWNER TO postgres;

--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE tickets; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tickets IS 'Tabla de los tickets de atención.';


--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN tickets.ticket_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets.ticket_id IS 'identificador de registro.';


--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN tickets.cod_ticket; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets.cod_ticket IS 'Valor generado mediante la combinación de la abreviatura del primero servicio del ticket y el número siguiente en secuencia para el servicio dado.';


--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN tickets.fecha_hora_ingreso_registro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets.fecha_hora_ingreso_registro IS 'Fecha y hora de ingreso del registro. Valor asignado mediante un DEFAULT para el atributo.,';


--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN tickets.nro_documento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets.nro_documento IS 'Número de documento de la persona que está solicitando atención.';


--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN tickets.prioridad_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets.prioridad_id IS 'Identificador de la prioridad asignada.';


--
-- TOC entry 223 (class 1259 OID 41251)
-- Name: tickets_servicios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tickets_servicios (
    ticket_id integer NOT NULL,
    item integer NOT NULL,
    servicio_id integer NOT NULL,
    fecha_hora_ingreso_registro timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    fecha_hora_inicio_atencion timestamp without time zone,
    fecha_hora_fin_atencion timestamp without time zone,
    estado_id integer NOT NULL,
    box_id_atencion integer DEFAULT 0
);


ALTER TABLE public.tickets_servicios OWNER TO postgres;

--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE tickets_servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tickets_servicios IS 'Tabla de los servicios asociados a un ticket.';


--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.ticket_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.ticket_id IS 'Identificador que hace referencia al ID del ticket al que corresponde.';


--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.item; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.item IS 'Número de item correlativo y correspondiente al ticket en cuestión. Este valor se generará de forma automáticamente mediante un trigger.';


--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.servicio_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.servicio_id IS 'Identificador del servicio asignado a un ticket.';


--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.fecha_hora_ingreso_registro; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.fecha_hora_ingreso_registro IS 'Fecha y hora de ingreso del registro. Generado automáticamente mediante un DEFAULT para el atributo.';


--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.fecha_hora_inicio_atencion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.fecha_hora_inicio_atencion IS 'Fecha y hora de inicio de la atención.';


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.fecha_hora_fin_atencion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.fecha_hora_fin_atencion IS 'Fecha y hora de finalización de la atención.';


--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.estado_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.estado_id IS 'Identificador del estado en el que se encuentra el pedido de servicio.';


--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN tickets_servicios.box_id_atencion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_servicios.box_id_atencion IS 'Identificador del box que ha aceptado la solicitud de atención. Valor DEFAULT es 0. Valor final se asigna al momento de que en el box se inicia la atención.';


--
-- TOC entry 222 (class 1259 OID 41124)
-- Name: tickets_socios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tickets_socios (
    ticket_id integer NOT NULL,
    socio_id integer NOT NULL
);


ALTER TABLE public.tickets_socios OWNER TO postgres;

--
-- TOC entry 3483 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE tickets_socios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tickets_socios IS 'Sub tipo de la tabla de tickets.';


--
-- TOC entry 3484 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN tickets_socios.ticket_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_socios.ticket_id IS 'Identificador del registro del ticket.';


--
-- TOC entry 3485 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN tickets_socios.socio_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tickets_socios.socio_id IS 'Identificador del registro del socio.';


--
-- TOC entry 218 (class 1259 OID 41105)
-- Name: tickets_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tickets_ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_ticket_id_seq OWNER TO postgres;

--
-- TOC entry 3486 (class 0 OID 0)
-- Dependencies: 218
-- Name: tickets_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tickets_ticket_id_seq OWNED BY public.tickets.ticket_id;


--
-- TOC entry 225 (class 1259 OID 41291)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    usuario_id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    login character varying(50) NOT NULL,
    clave_de_acceso character varying(100) NOT NULL,
    es_administrador boolean DEFAULT false NOT NULL,
    esta_activo boolean DEFAULT true NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 3487 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE usuarios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.usuarios IS 'Tabla de los usuarios del sistema.';


--
-- TOC entry 3488 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN usuarios.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.usuarios.usuario_id IS 'Identificador de registro de la tabla de usuarios.';


--
-- TOC entry 3489 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN usuarios.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.usuarios.nombre IS 'Nombre completo del usuario.';


--
-- TOC entry 3490 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN usuarios.login; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.usuarios.login IS 'Login de acceso del usuario.';


--
-- TOC entry 3491 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN usuarios.clave_de_acceso; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.usuarios.clave_de_acceso IS 'Clave de acceso del usuario que guardará un HASH MD5 para el valor correspondiente.';


--
-- TOC entry 3492 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN usuarios.es_administrador; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.usuarios.es_administrador IS 'Bandera de categoría de administrador del usuario.';


--
-- TOC entry 3493 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN usuarios.esta_activo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.usuarios.esta_activo IS 'Bandera de actividad del usuario.';


--
-- TOC entry 224 (class 1259 OID 41290)
-- Name: usuarios_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 3494 (class 0 OID 0)
-- Dependencies: 224
-- Name: usuarios_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_usuario_id_seq OWNED BY public.usuarios.usuario_id;


--
-- TOC entry 3215 (class 2604 OID 41090)
-- Name: boxes box_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boxes ALTER COLUMN box_id SET DEFAULT nextval('public.boxes_box_id_seq'::regclass);


--
-- TOC entry 3213 (class 2604 OID 41083)
-- Name: servicios servicio_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.servicios ALTER COLUMN servicio_id SET DEFAULT nextval('public.servicios_servicio_id_seq'::regclass);


--
-- TOC entry 3219 (class 2604 OID 41109)
-- Name: tickets ticket_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets ALTER COLUMN ticket_id SET DEFAULT nextval('public.tickets_ticket_id_seq'::regclass);


--
-- TOC entry 3225 (class 2604 OID 41294)
-- Name: usuarios usuario_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN usuario_id SET DEFAULT nextval('public.usuarios_usuario_id_seq'::regclass);


--
-- TOC entry 3249 (class 2606 OID 41132)
-- Name: apertura_cierre_boxes pk_aperturacierreboxes; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre_boxes
    ADD CONSTRAINT pk_aperturacierreboxes PRIMARY KEY (box_id, fecha_hora_apertura);


--
-- TOC entry 3243 (class 2606 OID 41092)
-- Name: boxes pk_boxes; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boxes
    ADD CONSTRAINT pk_boxes PRIMARY KEY (box_id);


--
-- TOC entry 3255 (class 2606 OID 41116)
-- Name: estados pk_estados; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estados
    ADD CONSTRAINT pk_estados PRIMARY KEY (estado_id);


--
-- TOC entry 3233 (class 2606 OID 41228)
-- Name: prioridades pk_prioridad; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prioridades
    ADD CONSTRAINT pk_prioridad PRIMARY KEY (prioridad_id);


--
-- TOC entry 3237 (class 2606 OID 41085)
-- Name: servicios pk_servicios; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.servicios
    ADD CONSTRAINT pk_servicios PRIMARY KEY (servicio_id);


--
-- TOC entry 3261 (class 2606 OID 41123)
-- Name: siguiente_nro_servicio pk_siguientenroservicio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siguiente_nro_servicio
    ADD CONSTRAINT pk_siguientenroservicio PRIMARY KEY (servicio_id, fecha);


--
-- TOC entry 3229 (class 2606 OID 41078)
-- Name: socios pk_socio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socios
    ADD CONSTRAINT pk_socio PRIMARY KEY (socio_id);


--
-- TOC entry 3251 (class 2606 OID 41111)
-- Name: tickets pk_tickets; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT pk_tickets PRIMARY KEY (ticket_id);


--
-- TOC entry 3267 (class 2606 OID 41256)
-- Name: tickets_servicios pk_tickets_servicios; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_servicios
    ADD CONSTRAINT pk_tickets_servicios PRIMARY KEY (ticket_id, item);


--
-- TOC entry 3263 (class 2606 OID 41144)
-- Name: tickets_socios pk_ticketsocios; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_socios
    ADD CONSTRAINT pk_ticketsocios PRIMARY KEY (ticket_id, socio_id);


--
-- TOC entry 3269 (class 2606 OID 41298)
-- Name: usuarios pk_usuarios; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT pk_usuarios PRIMARY KEY (usuario_id);


--
-- TOC entry 3245 (class 2606 OID 41156)
-- Name: boxes uq_boxes_abreviatura; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boxes
    ADD CONSTRAINT uq_boxes_abreviatura UNIQUE (abreviatura);


--
-- TOC entry 3247 (class 2606 OID 41154)
-- Name: boxes uq_boxes_descripcion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boxes
    ADD CONSTRAINT uq_boxes_descripcion UNIQUE (descripcion);


--
-- TOC entry 3257 (class 2606 OID 41189)
-- Name: estados uq_estados_cod_estado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estados
    ADD CONSTRAINT uq_estados_cod_estado UNIQUE (cod_estado);


--
-- TOC entry 3259 (class 2606 OID 41191)
-- Name: estados uq_estados_descripcion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estados
    ADD CONSTRAINT uq_estados_descripcion UNIQUE (descripcion);


--
-- TOC entry 3235 (class 2606 OID 41231)
-- Name: prioridades uq_prioridades_descripcion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prioridades
    ADD CONSTRAINT uq_prioridades_descripcion UNIQUE (descripcion);


--
-- TOC entry 3239 (class 2606 OID 41197)
-- Name: servicios uq_servicios_abreviatura; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.servicios
    ADD CONSTRAINT uq_servicios_abreviatura UNIQUE (abreviatura);


--
-- TOC entry 3241 (class 2606 OID 41243)
-- Name: servicios uq_servicios_descripcion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.servicios
    ADD CONSTRAINT uq_servicios_descripcion UNIQUE (descripcion);


--
-- TOC entry 3231 (class 2606 OID 41286)
-- Name: socios uq_socios_nro_documento; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socios
    ADD CONSTRAINT uq_socios_nro_documento UNIQUE (nro_documento);


--
-- TOC entry 3253 (class 2606 OID 41275)
-- Name: tickets uq_tickets_cod_ticket; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT uq_tickets_cod_ticket UNIQUE (cod_ticket);


--
-- TOC entry 3265 (class 2606 OID 41151)
-- Name: tickets_socios uq_tickets_socios_ticket_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_socios
    ADD CONSTRAINT uq_tickets_socios_ticket_id UNIQUE (ticket_id);


--
-- TOC entry 3271 (class 2606 OID 41302)
-- Name: usuarios uq_usuarios_login; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT uq_usuarios_login UNIQUE (login);


--
-- TOC entry 3495 (class 0 OID 0)
-- Dependencies: 3271
-- Name: CONSTRAINT uq_usuarios_login ON usuarios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT uq_usuarios_login ON public.usuarios IS 'Login de acceso del usuario.';


--
-- TOC entry 3273 (class 2606 OID 41300)
-- Name: usuarios uq_usuarios_nombre; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT uq_usuarios_nombre UNIQUE (nombre);


--
-- TOC entry 3496 (class 0 OID 0)
-- Dependencies: 3273
-- Name: CONSTRAINT uq_usuarios_nombre ON usuarios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT uq_usuarios_nombre ON public.usuarios IS 'Nombre del usuario.';


--
-- TOC entry 3276 (class 2606 OID 41213)
-- Name: apertura_cierre_boxes fk_apertura_cierre_boxes_boxes; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre_boxes
    ADD CONSTRAINT fk_apertura_cierre_boxes_boxes FOREIGN KEY (box_id) REFERENCES public.boxes(box_id);


--
-- TOC entry 3277 (class 2606 OID 41303)
-- Name: apertura_cierre_boxes fk_apertura_cierre_boxes_usuarios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.apertura_cierre_boxes
    ADD CONSTRAINT fk_apertura_cierre_boxes_usuarios FOREIGN KEY (usuario_id) REFERENCES public.usuarios(usuario_id);


--
-- TOC entry 3497 (class 0 OID 0)
-- Dependencies: 3277
-- Name: CONSTRAINT fk_apertura_cierre_boxes_usuarios ON apertura_cierre_boxes; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT fk_apertura_cierre_boxes_usuarios ON public.apertura_cierre_boxes IS 'Referencia a la tabla de usuarios.';


--
-- TOC entry 3274 (class 2606 OID 41198)
-- Name: boxes_servicios fk_boxes_servicios_boxes; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boxes_servicios
    ADD CONSTRAINT fk_boxes_servicios_boxes FOREIGN KEY (box_id) REFERENCES public.boxes(box_id);


--
-- TOC entry 3275 (class 2606 OID 41203)
-- Name: boxes_servicios fk_boxes_servicios_servicios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.boxes_servicios
    ADD CONSTRAINT fk_boxes_servicios_servicios FOREIGN KEY (servicio_id) REFERENCES public.servicios(servicio_id);


--
-- TOC entry 3279 (class 2606 OID 41208)
-- Name: siguiente_nro_servicio fk_siguiente_nro_servicio_servicios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.siguiente_nro_servicio
    ADD CONSTRAINT fk_siguiente_nro_servicio_servicios FOREIGN KEY (servicio_id) REFERENCES public.servicios(servicio_id);


--
-- TOC entry 3278 (class 2606 OID 41237)
-- Name: tickets fk_tickets_prioridades; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT fk_tickets_prioridades FOREIGN KEY (prioridad_id) REFERENCES public.prioridades(prioridad_id);


--
-- TOC entry 3498 (class 0 OID 0)
-- Dependencies: 3278
-- Name: CONSTRAINT fk_tickets_prioridades ON tickets; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT fk_tickets_prioridades ON public.tickets IS 'Referencia a la tabla de prioridades.';


--
-- TOC entry 3285 (class 2606 OID 41276)
-- Name: tickets_servicios fk_tickets_servicios_boxes; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_servicios
    ADD CONSTRAINT fk_tickets_servicios_boxes FOREIGN KEY (box_id_atencion) REFERENCES public.boxes(box_id);


--
-- TOC entry 3499 (class 0 OID 0)
-- Dependencies: 3285
-- Name: CONSTRAINT fk_tickets_servicios_boxes ON tickets_servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT fk_tickets_servicios_boxes ON public.tickets_servicios IS 'Referencia a la tabla de boxes.';


--
-- TOC entry 3284 (class 2606 OID 41269)
-- Name: tickets_servicios fk_tickets_servicios_estados; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_servicios
    ADD CONSTRAINT fk_tickets_servicios_estados FOREIGN KEY (estado_id) REFERENCES public.estados(estado_id);


--
-- TOC entry 3500 (class 0 OID 0)
-- Dependencies: 3284
-- Name: CONSTRAINT fk_tickets_servicios_estados ON tickets_servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT fk_tickets_servicios_estados ON public.tickets_servicios IS 'Referencia a la tabla de estados.';


--
-- TOC entry 3283 (class 2606 OID 41262)
-- Name: tickets_servicios fk_tickets_servicios_servicios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_servicios
    ADD CONSTRAINT fk_tickets_servicios_servicios FOREIGN KEY (servicio_id) REFERENCES public.servicios(servicio_id);


--
-- TOC entry 3501 (class 0 OID 0)
-- Dependencies: 3283
-- Name: CONSTRAINT fk_tickets_servicios_servicios ON tickets_servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT fk_tickets_servicios_servicios ON public.tickets_servicios IS 'Referencia a la tabla de los servicios.';


--
-- TOC entry 3282 (class 2606 OID 41257)
-- Name: tickets_servicios fk_tickets_servicios_tickets; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_servicios
    ADD CONSTRAINT fk_tickets_servicios_tickets FOREIGN KEY (ticket_id) REFERENCES public.tickets(ticket_id);


--
-- TOC entry 3502 (class 0 OID 0)
-- Dependencies: 3282
-- Name: CONSTRAINT fk_tickets_servicios_tickets ON tickets_servicios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON CONSTRAINT fk_tickets_servicios_tickets ON public.tickets_servicios IS 'Referencia a la tabla de los tickets.';


--
-- TOC entry 3280 (class 2606 OID 41138)
-- Name: tickets_socios fk_tickets_socios_socios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_socios
    ADD CONSTRAINT fk_tickets_socios_socios FOREIGN KEY (socio_id) REFERENCES public.socios(socio_id);


--
-- TOC entry 3281 (class 2606 OID 41145)
-- Name: tickets_socios fk_tickets_socios_tickets; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets_socios
    ADD CONSTRAINT fk_tickets_socios_tickets FOREIGN KEY (ticket_id) REFERENCES public.tickets(ticket_id);


-- Completed on 2022-12-24 19:04:33

--
-- PostgreSQL database dump complete
--

